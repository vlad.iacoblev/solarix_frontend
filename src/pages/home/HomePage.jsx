import React from "react";
import "./home.css";
import Bitcoin from "../../photos/bitcoin.png";
import { useNavigate } from "react-router-dom";

export const HomePage = () => {
  const navigate = useNavigate();

  const handleLearnMoreClick = () => {
    navigate("/cryptocurrencies");
  };
  return (
    <body>
      <div className="section">
        <div className="container">
          <div className="left">
            <h1 className="title">
              Welcome to
              <span
                style={{
                  display: "block",
                  color: "#a8b2b7",
                  fontSize: "100px",
                }}
              >
                Solarix
              </span>
            </h1>
            <p className="description">
              Join the future of finance with our cutting-edge crypto trading
              platform.{" "}
            </p>
            <button className="login-btn" onClick={() => navigate("/login")}>
              Login
            </button>
            <div className="log-reg">
              <div className="reg">
                <p className="reg-desc">Don't have an account?</p>
                <a href="/register" className="registerText">
                  Register here
                </a>
                <p className="reg-desc">Or</p>
                <a href="/cryptocurrencies" className="registerText">
                  Browse as guest
                </a>
              </div>
            </div>
          </div>
          <div className="right">
            <img src={Bitcoin} className="bitcoinImg" alt="Bitcoin " />
          </div>
        </div>
      </div>
    </body>
  );
};
