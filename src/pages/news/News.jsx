import { Link } from "react-router-dom";
import AccountBoxIcon from "@mui/icons-material/AccountBox";
import { NavBar } from "../../components/NavBar/NavBar";
import { LeftBar } from "../../components/LeftBar/LeftBar";
import "./news.css";
import ShidoChain from "../../photos/ShidoChain.jpg";
import eth from "../../photos/eth.jpeg";
export default function News() {
  return (
    <body>
      <div className="mainWrapper">
        <NavBar />
        <div className="mainWrapperContentNews">
          <div className="leftBar" id="leftBarNews">
            <Link to="/cryptocurrencies">
              <h1 className="cryptocurrencies">Cryptocurrencies</h1>
            </Link>
            <h1 className="myHistory" linkTo="/myHistory">
              My Trades
            </h1>
            <Link to="/news">
              <h1 className="news">News</h1>
            </Link>
          </div>
          <div className="mainContentNews">
            <div className="row1">
              <div className="wrapperNews"></div>
              <div className="wrapperNews"></div>
            </div>

            <div className="row2">
              <div className="wrapperNews">
                <img className="imgNews" src={ShidoChain}></img>
                <p className="paraNews">dsaddsad</p>
              </div>

              <div className="wrapperNews"></div>
            </div>
            <div className="row3">
              <div className="wrapperNews">
                <img className="imgNews" src={eth}></img>
                <div className="writingNews">
                  <h1 className="h1News">
                    Ethereum Price Analysis: ETH forms lower highs as it
                    struggles to break the $1,900 level
                  </h1>
                </div>
              </div>
              <div className="wrapperNews"></div>
            </div>
          </div>
        </div>
      </div>
    </body>
  );
}
