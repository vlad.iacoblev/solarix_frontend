import React from "react";
import AccountBoxIcon from "@mui/icons-material/AccountBox";
import { Link, useParams } from "react-router-dom";
import { useState, useEffect } from "react";
import "./allcrypto.css";
import axios from "axios";
import Coins from "../../components/Coins";
import cors from "cors";
import UserDropdown from "../../components/UserDropdown";
import { useNavigate } from "react-router-dom";
import { NavBar } from "../../components/NavBar/NavBar";

export default function AllCrypto() {
  const [searchText, setSearchText] = useState("");
  const navigate = useNavigate();
  const handleChange = (event) => {
    setSearchText(event.target.value);
  };
  const subimitSearchBar = (event) => {
    event.preventDefault();
    navigate(`/coins/${searchText}`);
  };
  const [openDrowDown, setopenDrowDown] = useState(false);

  return (
    <body>
      <div className="mainWrapper">
        <NavBar />
        <div className="mainContent">
          <div className="leftBar">
            <Link to="/cryptocurrencies" style={{ textDecoration: "none" }}>
              <h1 className="cryptocurrencies">Cryptocurrencies</h1>
            </Link>

            <Link to="/tradehistory" style={{ textDecoration: "none" }}>
              <h1 className="tradeHistoryName">My Trades</h1>
            </Link>
            {/* <Link to="/news">
              <h1 className="news">News</h1>
            </Link> */}
          </div>
          <div className="centerWrapperAllCrypto">
            <div className="cryptoListAllCrypto">
              <Coins />
            </div>
          </div>
        </div>
      </div>
    </body>
  );
}
