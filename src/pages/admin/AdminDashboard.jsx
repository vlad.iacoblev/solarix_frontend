import React from "react";
import { useState, useEffect } from "react";
import { PureComponent } from "react";
import "./admindashboard.css";
import { LeftBar } from "../../components/LeftBar/LeftBar";
import { NavBar } from "../../components/NavBar/NavBar";

import {
  BarChart,
  Bar,
  Cell,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
} from "recharts";

const data = [
  {
    name: "Page A",
    uv: 4000,
    pv: 2400,
    amt: 2400,
  },
  {
    name: "Page B",
    uv: 3000,
    pv: 1398,
    amt: 2210,
  },
  {
    name: "Page C",
    uv: 2000,
    pv: 9800,
    amt: 2290,
  },
  {
    name: "Page D",
    uv: 2780,
    pv: 3908,
    amt: 2000,
  },
  {
    name: "Page E",
    uv: 1890,
    pv: 4800,
    amt: 2181,
  },
  {
    name: "Page F",
    uv: 2390,
    pv: 3800,
    amt: 2500,
  },
  {
    name: "Page G",
    uv: 3490,
    pv: 4300,
    amt: 2100,
  },
];
export default function AdminDashboard() {
  const [allUsers, setAllUsers] = useState([]);
  const [statistics, setStatistics] = useState(data);
  //dsadas;

  useEffect(() => {
    const fetchAllUsers = async () => {
      try {
        const response = await fetch(
          `${process.env.REACT_APP_API_URL}/api/admin/getAllUsers`,
          {
            credentials: "include",
          }
        );
        const data = await response.json();
        setAllUsers(data);
      } catch (error) {
        console.error(error);
      }
    };

    fetchAllUsers();
  }, []);

  useEffect(() => {
    const registerStatistics = async () => {
      try {
        const response = await fetch(
          `${process.env.REACT_APP_API_URL}/api/admin/getRegisterStatistics/`,
          {
            credentials: "include",
          }
        );
        const data = await response.json();
        setStatistics(data);
        console.log(statistics);
        console.log(data);
      } catch (error) {
        console.error(error);
      }
    };

    registerStatistics();
  }, []);
  console.log(statistics);

  return (
    <div className="adminPage">
      <NavBar />
      <div className="contentWrapper">
        <div className="tableAdmin">
          <table>
            <thead>
              <tr>
                <th>Username</th>
                <th className="emailCol">Email</th>
                <th>First Name</th>
                <th>Last Name</th>
              </tr>
            </thead>
            <tbody>
              {allUsers.map((user) => (
                <tr key={user.id}>
                  <td>{user.username}</td>
                  <td>{user.email}</td>
                  <td>{user.firstName}</td>
                  <td>{user.lastName}</td>
                  <td>{user.password}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
        <div className="adminChart">
          <div>
            {/* <ResponsiveContainer width="100%" height="100%"> */}
            <BarChart width={750} height={300} data={statistics}>
              <YAxis dataKey="count" />
              <XAxis dataKey="date" />
              <Bar dataKey="count" fill="#8884d8" />
            </BarChart>
            {/* </ResponsiveContainer> */}
          </div>
        </div>
      </div>
    </div>
  );
}
