import React from "react";
import axios from "axios";
import { useRef } from "react";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { Alert, TextField } from "@mui/material";
import "./register.css";
import { Snackbar } from "@mui/material";
axios.defaults.withCredentials = true;

export default function RegisterPage() {
  const [username, setUsername] = useState("");
  const [firstName, setfirstName] = useState("");
  const [lastName, setlastName] = useState("");
  const [password, setPassword] = useState("");
  const [message, setMessage] = useState("");
  const [email, setEmail] = useState("");
  const [dbErrors, setDbErrors] = useState([]);
  const [openSnackBar, setOpenSnackBar] = useState(false);

  const navigate = useNavigate();

  const handleRegister = async (e) => {
    e.preventDefault();
    try {
      const createdAt = new Date();
      const res = await axios.post(
        process.env.REACT_APP_API_URL + "/api/auth/register",
        {
          username,
          firstName,
          lastName,
          email,
          password,
          createdAt,
        }
      );
      if (res.status === 200) {
        console.log(createdAt);
        setMessage("Successfully registered!");
        console.log("registration successful");
        navigate("/login");
      }
    } catch (error) {
      setMessage("Error: Registration failed");
      console.log(error.response.data);
      setDbErrors(Object.values(error.response.data));
      setOpenSnackBar(true);
    }
  };

  const testUser = async (e) => {
    const res = fetch("https://solarix-backend.wzt.ro/api/auth/register", {
      withCredntials: true,
      credentials: "include",
    });
    console.log(res);
  };

  return (
    <div className="registerSection">
      <div className="registerContainer">
        <div className="left-register">
          <h1 className="register-title-page">Register </h1>
          <p className="register-desc-page">
            Welcome to Solarix!
            <br /> By registering with us, you'll have access to a range of
            exciting features and tools to help you trade cryptocurrencies. Join
            our community today and start trading with confidence.
          </p>
          <p></p>
        </div>
        <div className="right-register">
          <div className="username-field-register">
            <TextField
              required
              id="outlined-basic"
              className="usernameField"
              label="Username"
              marginBottom="20px"
              variant="outlined"
              value={username}
              onChange={(e) => setUsername(e.target.value)}
              InputLabelProps={{ style: { color: "black" } }}
              inputProps={{ style: { color: "black" } }}
            />
          </div>

          <div className="firstname-field-register">
            <TextField
              required
              id="outlined-basic"
              label="First Name"
              variant="outlined"
              value={firstName}
              onChange={(e) => setfirstName(e.target.value)}
              InputLabelProps={{ style: { color: "black" } }}
              inputProps={{ style: { color: "black" } }}
            />
          </div>

          <div className="lastname-field-register">
            <TextField
              required
              id="outlined-basic"
              label="Last Name"
              variant="outlined"
              value={lastName}
              onChange={(e) => setlastName(e.target.value)}
              InputLabelProps={{ style: { color: "black" } }}
              inputProps={{ style: { color: "black" } }}
            />
          </div>

          <div className="email-field-register">
            <TextField
              required
              id="outlined-basic"
              label="Email"
              type="email"
              variant="outlined"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              InputLabelProps={{ style: { color: "black" } }}
              inputProps={{ style: { color: "black" } }}
            />
          </div>
          <div className="password-field-register">
            <TextField
              required
              id="outlined-basic"
              label="Password"
              type="password"
              variant="outlined"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              InputLabelProps={{ style: { color: "black" } }}
              inputProps={{ style: { color: "black" } }}
            />
          </div>
          <button
            className="registerButton-register"
            onClick={(e) => handleRegister(e)}
          >
            register
          </button>
          <br />
        </div>
      </div>
      <Snackbar
        open={openSnackBar}
        autoHideDuration={6000}
        //onClose={handleClose}
        message={dbErrors}
        // action={action}
        severity="error"
      >
        <Alert severity="error">{dbErrors}</Alert>
      </Snackbar>
    </div>
  );
}
