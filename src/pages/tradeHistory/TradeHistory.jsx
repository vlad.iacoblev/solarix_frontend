import React, { useEffect, useState } from "react";
import axios from "axios";
import { PureComponent } from "react";
import "./tradehistory.css";

export default function TradeHistory() {
  const [tradeUser, setTradeUser] = useState([]);

  useEffect(() => {
    const fetchTradeHistory = async () => {
      try {
        const response = await fetch(
          `${process.env.REACT_APP_API_URL}/api/trades/UserTrades`,
          {
            credentials: "include",
          }
        );
        const data = await response.json();
        console.log(data);
        setTradeUser(data);
      } catch (error) {
        console.error("Error fetching trade history:", error);
      }
    };

    fetchTradeHistory();
  }, []);
  console.log(tradeUser);

  return (
    <div className="tradeHistorySection">
      <div className="tradeHistoryContainer">
        <div className="tradeHistoryLeft">
          <h1 className="tradeHistoryTitle">Trade History</h1>
          <br></br>
          <p>
            {" "}
            Each entry includes essential details such as the date and time of
            the trade, the specific cryptocurrency pairs involved, the quantity
            of assets bought or sold, and the corresponding prices at the time
            of execution.
          </p>
        </div>
        <div className="tradeHistoryRight">
          <ul>
            {tradeUser.map((trade) => (
              <li className="liElement" key={trade._id}>
                {trade.username} traded:{trade.fromAmount.toFixed(2)}{" "}
                {trade.fromSymbol} for:
                {trade.toAmount.toFixed(2)} {trade.toSymbol} at{" "}
                {new Date(trade.timestamp).toLocaleString()}
              </li>
            ))}
          </ul>
        </div>
      </div>
    </div>
  );
}
