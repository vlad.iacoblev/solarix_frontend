import axios from "axios";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import CoinItem from "../../components/CoinItem";
import "./coinpage.css";
import { NavBar } from "../../components/NavBar/NavBar";
import { PureComponent } from "react";
import {
  AreaChart,
  Area,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  ResponsiveContainer,
  LineChart,
  Line,
} from "recharts";
import { Legend } from "recharts";

const data = [
  {
    name: "Page A",
    uv: 4000,
    pv: 2400,
    amt: 2400,
  },
  {
    name: "Page B",
    uv: 3000,
    pv: 1398,
    amt: 2210,
  },
  {
    name: "Page C",
    uv: 2000,
    pv: 9800,
    amt: 2290,
  },
  {
    name: "Page D",
    uv: 2780,
    pv: 3908,
    amt: 2000,
  },
  {
    name: "Page E",
    uv: 1890,
    pv: 4800,
    amt: 2181,
  },
  {
    name: "Page F",
    uv: 2390,
    pv: 3800,
    amt: 2500,
  },
  {
    name: "Page G",
    uv: 3490,
    pv: 4300,
    amt: 2100,
  },
];

const CoinPage = () => {
  const { coinSymbol } = useParams();
  const [coin, setCoin] = useState(null);
  const [amount, setAmount] = useState(0);
  const [coinData, setCoinData] = useState(null);
  const [refresh, setRefresh] = useState(false);

  useEffect(() => {
    const getCoin = async () => {
      try {
        const res = await axios.get(
          process.env.REACT_APP_API_URL + "/api/crypto/getOne/" + coinSymbol
        );

        setCoin(res.data);
      } catch (err) {
        console.log(err);
      }
    };
    getCoin();
  }, [coinSymbol, refresh]);

  useEffect(() => {
    const getCoinData = async () => {
      try {
        const res = await axios.get(
          process.env.REACT_APP_API_URL + "/api/crypto/getHistory/" + coinSymbol
        );
        setCoinData(res.data);
      } catch (err) {
        console.log(err);
      }
    };
    getCoinData();
  }, [coinSymbol, refresh]);

  const handleBuyClick = async () => {
    try {
      const response = await axios.post(
        process.env.REACT_APP_API_URL + "/api/crypto/buyCrypto",
        {
          symbol: coin.symbol,
          amount: amount,
        }
      );

      console.log(response.data);
      setRefresh(!refresh);
    } catch (err) {
      console.log(err);
    }
  };

  const handleSellClick = async () => {
    try {
      const response = await axios.post(
        process.env.REACT_APP_API_URL + "/api/crypto/sellCrypto",
        {
          symbol: coin.symbol,
          amount: amount,
        }
      );
      console.log(response.data.message);
      setRefresh(!refresh);
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <div className="coinPageWrapper">
      <NavBar />
      <div className="coinPageContentWrapper">
        <div className="leftCoinPage">
          <div className="chartWrapper">
            <ResponsiveContainer width="100%" height="100%">
              <LineChart
                width={500}
                height={400}
                data={coinData}
                margin={{
                  top: 30,
                  right: 30,
                  left: 30,
                  bottom: 30,
                }}
              >
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis
                  dataKey="timestamp"
                  tick={{ fontSize: 12, padding: 5 }}
                  tickFormatter={(tick) =>
                    new Date(tick).toLocaleDateString("en-US", {
                      month: "numeric",
                      day: "numeric",
                      year: "numeric",
                    })
                  }
                />
                <YAxis dataKey="priceInUSD" />
                <Tooltip />
                <Line
                  type="linear"
                  dataKey="priceInUSD"
                  stroke="#8884d8"
                  strokeWidth={5}
                  dot={false}
                  animationDuration={1000}
                  animationEasing="ease-out"
                />
              </LineChart>
            </ResponsiveContainer>
          </div>
          <div className="buttonWrapper">
            <button className="buyButton" onClick={handleBuyClick}>
              Buy
            </button>
            <input
              className="amountInput"
              type="number"
              value={amount}
              onChange={(e) => setAmount(Math.max(0, e.target.value))}
            />
            <button className="sellButton" onClick={handleSellClick}>
              Sell
            </button>
          </div>
        </div>

        <div className="rightCoinPage">
          <img className="cryptoImgCoinPage" src={`${coin?.logo}`} />
          <h1>{coin?.name}</h1>
          <h1>{coin?.symbol}</h1>
          <h1>Current price: {coin?.priceInUSD.toFixed(2)} $ </h1>
          <p>{coin?.description}</p>
          <h1>
            Market Cap:{" "}
            {coin?.marketCap &&
              (coin.marketCap >= 1e9
                ? `${(coin.marketCap / 1e9).toFixed(1)}B`
                : coin.marketCap >= 1e6
                ? `${(coin.marketCap / 1e6).toFixed(1)}M`
                : `${coin.marketCap}`) + " $"}
          </h1>
          <h1>
            Circulating Supply:{" "}
            {coin?.circulatingSupply &&
              (coin?.circulatingSupply >= 1e9
                ? `${(coin.circulatingSupply / 1e9).toFixed(1)}B`
                : coin.marketCap >= 1e6
                ? `${(coin.circulatingSupply / 1e6).toFixed(1)}M`
                : `${coin.circulatingSupply}`) + " $"}
          </h1>
        </div>
      </div>
    </div>
  );
};

export default CoinPage;
