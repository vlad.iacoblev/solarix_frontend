import React, { useEffect, useState } from "react";
import axios from "axios";
import { NavBar } from "../../components/NavBar/NavBar";
import "./profilepage.css";
import { TextField } from "@mui/material";
function ProfilePage() {
  const [user, setUserData] = useState(null);
  const [ccNumber, setCcNumber] = useState("");
  const [ccExpDate, setCcExpDate] = useState("");
  const [ccCVV, setCcCVV] = useState("");
  const [ccName, setCcName] = useState("");
  const [message, setMessage] = useState("");
  const [amount, setAmount] = useState(0);
  const [walletSum, setWalletSum] = useState(0);

  const handleWallet = async (e) => {
    e.preventDefault();
    try {
      const res = await axios.post(
        process.env.REACT_APP_API_URL + "/api/user/setCard",
        {
          ccNumber,
          ccExpDate,
          ccCVV,
          ccName,
        }
      );
    } catch (error) {
      setMessage("Error: Wallet failed");
    }
    console.log(ccNumber, ccExpDate, ccCVV, ccName);
  };

  const handleAddDeposit = async () => {
    try {
      const response = await axios.post(
        process.env.REACT_APP_API_URL + "/api/wallet/deposit",
        {
          amount: amount,
        }
      );
      console.log(response.data.message);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    const fetchWalletSum = async () => {
      try {
        const res = await axios.get(
          process.env.REACT_APP_API_URL + "/api/wallet/getSum"
        );
        setWalletSum(res.data);
      } catch (err) {
        console.log(err);
      }
    };
    fetchWalletSum();
  }, []);
  console.log(walletSum);

  useEffect(() => {
    const fetchUserData = async () => {
      try {
        const response = await fetch(
          `${process.env.REACT_APP_API_URL}/api/user/getUser`,
          {
            credentials: "include",
          }
        );
        const data = await response.json();
        setUserData(data);
      } catch (error) {
        console.error(error);
      }
    };

    fetchUserData();
  }, []);

  return (
    <div className="profilePageContainer">
      {user ? (
        <div className="profilePageContent">
          <div className="accountInfo">
            <h1 className="accountInfoTitle">Profile Information</h1>
            <div className="userInfor">
              <p>
                <span style={{ fontWeight: "bold" }}>Username:</span>{" "}
                {user.username}
              </p>
              <p>
                <span style={{ fontWeight: "bold" }}>Email:</span> {user.email}
              </p>
              <p>
                <span style={{ fontWeight: "bold" }}>First Name:</span>{" "}
                {user.firstName}
              </p>
              <p>
                <span style={{ fontWeight: "bold" }}>Last Name:</span>{" "}
                {user.lastName}
              </p>
              <p>
                <span style={{ fontWeight: "bold" }}>Balance:</span>{" "}
                {JSON.stringify(walletSum.WalletUSD)} USD
              </p>
            </div>
          </div>
          <div profilePage-right>
            <div className="addCard">
              <h2 className="addCardTitle">Add Card</h2>
              <div className="creditCardNumberInput">
                <TextField
                  required
                  id="outlined-basic"
                  label="Credit Card Number"
                  variant="outlined"
                  value={ccNumber}
                  onChange={(e) => setCcNumber(e.target.value)}
                />
              </div>
              <div className="creditCardExpDate">
                <TextField
                  required
                  className="creditCardExpDate"
                  id="outlined-basic"
                  label="Expiration Date"
                  variant="outlined"
                  value={ccExpDate}
                  onChange={(e) => setCcExpDate(e.target.value)}
                />
              </div>
              <div className="creditCardCvv">
                <TextField
                  required
                  id="outlined-basic"
                  label="CVV"
                  variant="outlined"
                  value={ccCVV}
                  onChange={(e) => setCcCVV(e.target.value)}
                />
              </div>
              <div className="creditCardCardName">
                <TextField
                  required
                  id="outlined-basic"
                  label="Card Name"
                  variant="outlined"
                  value={ccName}
                  onChange={(e) => setCcName(e.target.value)}
                />
              </div>
              <button
                className="addCardbtn"
                type="submit"
                onClick={handleWallet}
              >
                ADD CARD
              </button>
            </div>
            <div className="addMoney">
              <h2 className="addMoney-title">Add Money</h2>
              <input
                type="number"
                value={amount}
                onChange={(e) => setAmount(Math.max(0, e.target.value))}
              />
              <button
                className="addMoneybtn"
                type="submit"
                onClick={handleAddDeposit}
              >
                ADD Money
              </button>
            </div>
          </div>
        </div>
      ) : (
        <p>Loading user data...</p>
      )}
    </div>
  );

  // return (
  //   <div className="profileSection">
  //     <div className="profileContainer">
  //       <div className="left-profile">
  //         <h1 className="profile-title-page">Profile </h1>
  //          <div className="accountInfo">
  //   <p>Username: {user.username}</p>
  //   <p>Email: {user.email}</p>
  //   <p>First Name: {user.firstName}</p>
  //   <p>Last Name: {user.lastName}</p>
  //   <p>Balance: {JSON.stringify(walletSum.WalletUSD)}</p>
  // </div>
  //
  //       </div>
  //       <div className="right-profile">
  //         <div className="creditCard-name">
  //          <TextField
  //   required
  //   id="outlined-basic"
  //   label="Credit Card Number"
  //   variant="outlined"
  //   value={ccNumber}
  //   onChange={(e) => setCcNumber(e.target.value)}
  //   InputLabelProps={{ style: { color: "white" } }}
  //   InputProps={{
  //     classes: {
  //       notchedOutline: "whiteBorder",
  //     },
  //   }}
  // />
  //         </div>
  //         {/* <input
  //           placeholder="Username"
  //           type="username"
  //           value={username}
  //           onChange={(e) => setUsername(e.target.value)}
  //         /> */}
  //         <div className="creditCard-cvv"></div>
  //          <TextField
  //   required
  //   id="outlined-basic"
  //   label="CVV"
  //   variant="outlined"
  //   value={ccCVV}
  //   onChange={(e) => setCcCVV(e.target.value)}
  //   InputLabelProps={{ style: { color: "white" } }}
  //   InputProps={{
  //     classes: {
  //       notchedOutline: "whiteBorder",
  //     },
  //   }}
  // />
  //         {/* <input
  //           placeholder="Password"
  //           type="password"
  //           value={password}
  //           onChange={(e) => setPassword(e.target.value)}
  //         /> */}

  //         <button type="submit" onClick={(e) => handleAddDeposit(e)}>
  // ADD money
  // </button>
  //         <br />
  //       </div>
  //     </div>
  //   </div>
  // );
}

export default ProfilePage;
