import React, { useEffect, useState } from "react";
import axios from "axios";
import { useParams } from "react-router-dom";
import { Alert } from "@mui/material";
import { Snackbar } from "@mui/material";
import "./portofolio.css";
import { TextField } from "@mui/material";
function AlertF(props) {
  return <Alert elevation={6} variant="filled" {...props} />;
}
export default function Portofolio() {
  const [portfolio, setPortfolio] = useState(null);
  const [formData, setFormData] = useState({
    fromSymbol: "",
    fromAmount: "",
    toSymbol: "",
  });
  const [openSnackBar, setOpenSnackBar] = useState(false);
  const [snackbarMessage, setSnackbarMessage] = useState("");
  const [snackbarSeverity, setSnackbarSeverity] = useState("");

  useEffect(() => {
    const fetchPortfolio = async () => {
      try {
        const res = await axios.get(
          `${process.env.REACT_APP_API_URL}/api/wallet/getUserPortfolio`
        );
        setPortfolio(res.data);
      } catch (err) {
        console.log(err);
      }
    };
    fetchPortfolio();
  }, []);

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setFormData((prevFormData) => ({ ...prevFormData, [name]: value }));
  };

  const handleSendButtonClick = async () => {
    try {
      const res = await axios.post(
        `${process.env.REACT_APP_API_URL}/api/crypto/tradeCrypto`,
        formData
      );
      setOpenSnackBar(true); // add this line to open the Snackbar
      setSnackbarMessage(
        `Traded ${formData.fromAmount} ${formData.fromSymbol} to ${res.data.toAmount} ${formData.toSymbol} successfully`
      ); // set the Snackbar message with the traded information
      setSnackbarSeverity("success"); // set the severity to success
      // Only update the portfolio state if the trade was successful
      // setPortfolio(res.data);
    } catch (err) {
      console.log(err);
      setOpenSnackBar(true); // add this line to open the Snackbar
      setSnackbarMessage(err.response.data.message); // set the Snackbar message with the error message returned from the backend
      setSnackbarSeverity("error"); // set the severity to error
    }
  };

  return (
    <div className="portfolioWrapper">
      <div className="portfolioContent">
        <div className="portfolioContent-left">
          <h1 className="portfolio-title">My portfolio</h1>
          {portfolio && (
            <table style={{ color: "black" }}>
              <thead>
                <tr>
                  <th style={{ paddingRight: "70px", fontSize: "25px" }}>
                    Asset
                  </th>
                  <th style={{ fontSize: "25px" }}>Quantity</th>
                </tr>
              </thead>
              <tbody>
                {Object.entries(portfolio).map(([asset, quantity]) => (
                  <tr key={asset}>
                    <td style={{ fontSize: "15px" }}>{asset}</td>
                    <td>{quantity.toFixed(2)}</td>
                  </tr>
                ))}
              </tbody>
            </table>
          )}
        </div>
        <div className="portfolioContent-right">
          <p className="portfolioDesc">
            Trading pairs are a fundamental concept in the cryptocurrency
            market, allowing you to exchange one digital asset for another.
          </p>
          <div className="tradePanel">
            <input
              placeholder="from crypto"
              name="fromSymbol"
              value={formData.fromSymbol}
              onChange={handleInputChange}
            />
            <br />
            {/* <TextField
              required
              id="outlined-basic"
              label="from crypto"
              variant="outlined"
              value={formData.fromSymbol}
            /> */}
            <input
              placeholder="to crypto"
              name="toSymbol"
              value={formData.toSymbol}
              onChange={handleInputChange}
            />
            <br />
            <input
              placeholder="amount"
              name="fromAmount"
              value={formData.fromAmount}
              onChange={handleInputChange}
            />
            <br />
            <button className="tradeBtn" onClick={handleSendButtonClick}>
              Send
            </button>
            <Snackbar
              open={openSnackBar}
              autoHideDuration={6000}
              onClose={() => setOpenSnackBar(false)}
              message={snackbarMessage}
              severity={snackbarSeverity}
            >
              <Alert severity={snackbarSeverity}>{snackbarMessage}</Alert>
            </Snackbar>
          </div>
        </div>
      </div>
    </div>
  );
}
