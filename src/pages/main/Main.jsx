import React from 'react'
import "./main.css";
import AccountBoxIcon from '@mui/icons-material/AccountBox';
import { Link } from 'react-router-dom';
import  { useState } from 'react';
import { LeftBar } from '../../components/LeftBar/LeftBar';
import {NavBar} from '../../components/NavBar/NavBar';


const DropDown=()=>{
    return(
        <div className='dropDownWrapper'>
        <ul className='dropDown'>
            <li>Welcome,Vlad</li>
            <Link to="/news"><li>vlad@yahoo.com</li></Link>
            <li>Profile</li>
            <li>My portofolio</li>
        </ul>
        </div>
    )
}

export default function Main() {

    const[openDrowDown,setopenDrowDown]=useState(false)
  return (
    <div className='mainWrapper'>
         <NavBar/>
        <div className='mainContent'>
            <LeftBar/>
            <div className='centerWrapper'>
                <div className="balanceWrapper">
                    <h1>My balance</h1>
                </div>
                <div className="cryptoList">
                    <h1>Top movers:</h1>
                    <div className='topMoversWrapper'>
                        <div className='card1'></div>
                        <div className='card'></div>
                        <div className='card'></div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
  )
}


