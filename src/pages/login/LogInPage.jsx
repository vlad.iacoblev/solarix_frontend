import React from "react";
import "./login.css";
import axios from "axios";
import { useRef } from "react";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { TextField } from "@mui/material";
axios.defaults.withCredentials = true;

export default function LogInPage() {
  // const [email, setEmail] = useState("");
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [message, setMessage] = useState("");
  const navigate = useNavigate();

  const handleLogin = async (e) => {
    console.log("test");
    e.preventDefault();

    try {
      const res = await axios.post(
        process.env.REACT_APP_API_URL + "/api/auth/login",
        {
          username,
          password,
        }
      );

      if (res.status === 200) {
        setMessage("Successfully logged in!");
        console.log("login successful");
        console.log(res.data);
        if (res.data.isAdmin === true) {
          navigate("/admin", { replace: true });
        } else if (res.data.isAdmin === false) {
          navigate("/cryptocurrencies", { replace: true });
        }
      }
    } catch (error) {
      setMessage("Error: Login failed");
    }
  };
  const testUser = async (e) => {
    const res = fetch(process.env.REACT_APP_API_URL + "/api/user/getUser", {
      withCredntials: true,
      credentials: "include",
    });
    console.log(res);
  };

  return (
    <div className="loginSection">
      <div className="loginContainer">
        <div className="left-login">
          <h1 className="login-title-page">Login </h1>
          <p className="login-desc-page">
            Welcome to Solarix login page. To access your personal account,
            please enter your username and password.
          </p>
          <p></p>
        </div>
        <div className="right-login">
          <div className="username-field-login">
            <TextField
              required
              id="outlined-basic"
              className="usernameField"
              label="Username"
              marginBottom="20px"
              variant="outlined"
              value={username}
              onChange={(e) => setUsername(e.target.value)}
              InputLabelProps={{ style: { color: "black" } }}
              inputProps={{ style: { color: "black" } }}
            />
          </div>
          {/* <input
            placeholder="Username"
            type="username"
            value={username}
            onChange={(e) => setUsername(e.target.value)}
          /> */}
          <div className="password-field-login"></div>
          <TextField
            required
            id="outlined-basic"
            label="Password"
            type="password"
            variant="outlined"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            InputLabelProps={{ style: { color: "black" } }}
            inputProps={{ style: { color: "black" } }}
          />
          {/* <input
            placeholder="Password"
            type="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          /> */}

          <button className="loginButton-login" onClick={(e) => handleLogin(e)}>
            login
          </button>
          <br />
        </div>
      </div>
    </div>
  );
}
