import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { HomePage } from "./pages/home/HomePage";
import LogInPage from "./pages/login/LogInPage";
import RegisterPage from "./pages/register/RegisterPage";
import Main from "./pages/main/Main";
import News from "./pages/news/News";
import AllCrypto from "./pages/allcrypto/AllCrypto";
import Coins from "./components/Coins";
import CoinItem from "./components/CoinItem";
import CoinPage from "./pages/coinpage/CoinPage";
import { NavBar } from "./components/NavBar/NavBar";
import ProfilePage from "./pages/profile/ProfilePage";
import AdminDashboard from "./pages/admin/AdminDashboard";
import Portofolio from "./pages/portofolio/Portofolio";
import TradeHistory from "./pages/tradeHistory/TradeHistory";
function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<HomePage />}></Route>
        <Route path="/login" element={<LogInPage />}></Route>
        <Route path="/register" element={<RegisterPage />}></Route>
        <Route path="/main" element={<Main />}></Route>
        <Route path="/news" element={<News />}></Route>
        <Route path="/cryptocurrencies" element={<AllCrypto />}></Route>
        <Route path="/coins/:coinSymbol" element={<CoinPage />} />
        <Route path="/profile/:userId" element={<ProfilePage />} />
        <Route path="/portofolio/:userId" element={<Portofolio />} />
        <Route path="/admin" element={<AdminDashboard />}></Route>
        <Route path="/tradehistory" element={<TradeHistory />}></Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
