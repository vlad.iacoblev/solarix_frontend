import React from "react";
import { useNavigate } from "react-router-dom";

const CoinItem = (props) => {
  const navigate = useNavigate();
  const pressedCoin = (event) => {
    event.preventDefault();
    navigate(`/coins/${props.coinItem.symbol}`);
  };

  return (
    <div className="coin-row" onClick={pressedCoin}>
      <div className="imageCryptoWrap">
        <img className="imageCrypto" src={props.coinItem.logo} alt=" " />
        <p>{props.coinItem.symbol}</p>
      </div>

      <p>{props.coinItem.name} </p>
      <p>${Number(props.coinItem.priceInUSD).toFixed(2)}</p>
      {/* <p>0%</p> */}
      {/* <p>${Number(props.coinItem.totalSupply).toFixed(0)}M</p> */}
      <p>
        {props.coinItem.totalSupply &&
          (props.coinItem.totalSupply >= 1e9
            ? `${(props.coinItem.totalSupply / 1e9).toFixed(1)}B`
            : props.coinItem.totalSupply >= 1e6
            ? `${(props.coinItem.totalSupply / 1e6).toFixed(1)}M`
            : `${props.coinItem.totalSupply}`) + " $"}
      </p>
      <p>
        {props.coinItem.marketCap &&
          (props.coinItem.marketCap >= 1e9
            ? `${(props.coinItem.marketCap / 1e9).toFixed(1)}B`
            : props.coinItem.marketCap >= 1e6
            ? `${(props.coinItem.marketCap / 1e6).toFixed(1)}M`
            : `${props.coinItem.marketCap}`) + " $"}
      </p>
    </div>
  );
};

export default CoinItem;
