import { Link } from "react-router-dom"

export const LeftBar = () =>{
    return <div className='leftBar'>
    <h1 className='mainPage' linkTo="/main">Home</h1>
    <Link to="/cryptocurrencies"><h1 className='cryptocurrencies' >Cryptocurrencies</h1></Link>
    <h1 className='myHistory' linkTo="/myHistory">My Trades</h1>
    <Link to="/news"><h1 className='news' >News</h1></Link>
</div>
}