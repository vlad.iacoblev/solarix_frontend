import React, { useState } from "react";
import AccountBoxIcon from "@mui/icons-material/AccountBox";
import "./NavBar.css";
import { useNavigate } from "react-router-dom";
import UserDropdown from "../../components/UserDropdown";
import axios from "axios";
import { useEffect } from "react";

export const NavBar = () => {
  const [searchText, setSearchText] = useState("");
  const navigate = useNavigate();
  const [walletSum, setWalletSum] = useState(0);

  const handleChange = (event) => {
    setSearchText(event.target.value.toUpperCase());
  };
  const subimitSearchBar = (event) => {
    event.preventDefault();
    navigate(`/coins/${searchText}`);
  };
  const [openDrowDown, setopenDrowDown] = useState(false);
  const [data, setData] = useState(null);

  useEffect(() => {
    const fetchWalletSum = async () => {
      try {
        const res = await axios.get(
          process.env.REACT_APP_API_URL + "/api/wallet/getSum"
        );
        const sum = Number(res.data.WalletUSD); // convert WalletUSD to a number
        setWalletSum(sum.toFixed(2)); // set walletSum to a string with two decimal places
      } catch (err) {
        console.log(err);
      }
    };
    fetchWalletSum();
  }, []);

  console.log(walletSum);

  return (
    <div className="navbar">
      <div className="searchBarWrapper">
        <form onSubmit={subimitSearchBar}>
          <input
            type="text"
            className="searchBar"
            placeholder="Search for your crypto"
            onChange={handleChange}
          />
        </form>
      </div>
      <div className="amountWrapper">
        <h1 className="balanceh1">My Balance: {walletSum} $</h1>
      </div>
      <div className="profileWrrapper">
        <div className="profileIcon">
          <AccountBoxIcon
            style={{ fontSize: "50px", color: "white" }}
            onClick={() => setopenDrowDown((prev) => !prev)}
          />
        </div>
        {openDrowDown && (
          <div className="dropDownWrapper">
            <UserDropdown />
          </div>
        )}
      </div>
    </div>
  );
};
