import React from "react";
import CoinItem from "./CoinItem";
import { Link, useNavigate } from "react-router-dom";
import AllCrypto from "../pages/allcrypto/AllCrypto";
import CoinPage from "../pages/coinpage/CoinPage";
import { useState, useEffect } from "react";

const Coins = () => {
  const [data, setData] = useState(null);
  const navigate = useNavigate();

  useEffect(() => {
    async function fetchData() {
      const response = await fetch(
        process.env.REACT_APP_API_URL + "/api/crypto/getAll"
      );
      const json = await response.json();
      setData(json);
    }

    fetchData();
  }, []);

  if (!data) {
    return <p>Loading data...</p>;
  }

  return (
    <div className="cryptoListWrapper">
      <div className="crypto-header" style={{ color: "white" }}>
        <p>Symbol</p>
        <p className="coinName">Name</p>
        <p className="coinPrice">Price</p>
        {/* <p>24h</p> */}
        <p>Volume</p>
        <p>Market Cap</p>
      </div>

      {Object.keys(data).map((key) => {
        const coin = data[key];
        if (key === "message") return;
        return <CoinItem coinItem={coin} key={coin._id} />;
      })}
    </div>
  );
};

export default Coins;
