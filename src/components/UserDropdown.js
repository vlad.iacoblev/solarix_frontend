import React from "react";
import { Link, useNavigate } from "react-router-dom";
import { useState, useEffect } from "react";

const UserDropdown = (props) => {
  const [data, setData] = useState(null);
  const navigate = useNavigate();

  const logout = () => {
    const response = fetch(process.env.REACT_APP_API_URL + "/api/auth/logout", {
      method: "POST",
      withCredntials: true,
      credentials: "include",
    });
    setData(null);
    navigate("/login");
  };

  useEffect(() => {
    async function fetchData() {
      const response = await fetch(
        process.env.REACT_APP_API_URL + "/api/user/getUser",
        {
          withCredntials: true,
          credentials: "include",
        }
      );
      const json = await response.json();
      setData(json);
      console.log(json);
    }

    fetchData();
  }, []);

  if (!data || !data.username) {
    return (
      <div className="dropDownWrapper">
        <ul className="dropDown">
          <li> Not logged in </li>
          <li>
            {" "}
            <a href="/login"> To login page... </a>{" "}
          </li>
        </ul>
      </div>
    );
  }

  return (
    <div className="dropDownWrapper">
      <ul className="dropDown">
        <li> Welcome, {data.username} </li>

        <Link
          to={`/profile/${data.id}`}
          style={{ textDecoration: "none", color: " #253f4b" }}
        >
          <li>Profile</li>
        </Link>
        <Link
          to={`/portofolio/${data.id}`}
          style={{ textDecoration: "none", color: " #253f4b" }}
        >
          <li>My portofolio</li>
        </Link>

        <li onClick={logout}>Logout</li>
      </ul>
    </div>
  );
};

export default UserDropdown;
